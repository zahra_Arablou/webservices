CREATE DATABASE AuthorBook;
use AuthorBook;

CREATE TABLE IF NOT EXISTS `books` (
  `id` int NOT NULL AUTO_INCREMENT ,
  `authorId` INT NOT NULL,
  `title` varchar(50) NOT NULL,
  `subtitle` varchar(50) NOT NULL,
  `reviews` int NOT NULL,
  `expert_review` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `publishDate` DateTime NOT NULL,
   PRIMARY KEY (`id`));

INSERT INTO `books` ( `authorId`,`title`,`subtitle`,`reviews`,`expert_review`,`description`,`publishDate`) VALUES
( 1,'Hamlet','An awesome book for awesome people',5,'awsome book, quick read','This is a book for philosophy lovers.','2010-02-01' ),
( 3,'Kids','An awesome book for parents',3,'educational,practical, quick read','This is a book for parents who want to grow their chid perfectly.','2010-02-01' ),
(  2,'The Tempest','Another awesome book by your own Shakespeare',3,'fantastic, quick read','This is a book for philosophy lovers.','2012-02-01'  );

CREATE TABLE IF NOT EXISTS `authors` (
  `id` int NOT NULL AUTO_INCREMENT ,
   `email` varchar(50) NOT NULL ,
    `name` varchar(50) NOT NULL,
    `writing_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`));
  
INSERT INTO `authors` (  `email`,`name`,`writing_type`) VALUES
( 'daniel@gmail.com','Daniel Vanda','Romantic'),
( 'Kasia@yahoo.com','Kasia Dutin','Fiction'),
( 'mery@gmail.com','Mery Vanda','educational');



  
ALTER TABLE `books` 
ADD CONSTRAINT `authorId`
  FOREIGN KEY (`authorId`)
  REFERENCES `authors` (`id`);
  
  
  


select * from books;
select * from authors;