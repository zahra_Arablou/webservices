var express = require('express');
var router = express.Router();
const sql =require('../db');
const Joi=require('joi');
const validator =require('express-joi-validation').createValidator({passError:true});

//validation
const validationSchema=Joi.object({
  name:Joi.string().min(3).max(30).required(),
  email: Joi.string().regex(/^[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+$/).required(),
  writing_type:Joi.string().min(3).max(50).required()
});
/* GET users listing. */
router.get('/', validator.fields(validationSchema),function(req, res, next) {
  sql.query("select * from authors",function(error,results,fields){
    if(error)
    { 
        next(error);
        return;
    }
    res.json(results);
  });
 // res.send('respond with a resource');
});

module.exports = router;
