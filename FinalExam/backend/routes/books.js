var express = require('express');
var router = express.Router();
const sql =require('../db');
const Joi=require('joi');
const validator =require('express-joi-validation').createValidator({passError:true});


const validationSchema=Joi.object({
    title:Joi.string().min(3).max(50).required(),
    authorId:Joi.number().required(),
    subtitle:Joi.string().min(3).max(100).required(),
    reviews:Joi.number().required(),
    expert_review:Joi.string().min(3).max(200).required(),
    description:Joi.string().min(3).max(250).required(),
    publishDate:Joi.required()
   });
/* GET users listing. */
router.get('/', validator.fields(validationSchema), function(req, res, next) {
  sql.query("select * from books",function(error,results,fields){
    if(error)
           { 
               next(error);
               return;
           }
         
    res.json(results);
  });
 // res.send('respond with a resource');
});

/* GET spesific author books by author id */
router.get('/:id',validator.fields(validationSchema), function(req, res, next) {
    sql.query("select * from books where authorId=?",[req.params.id],function(error,results,fields){
        if(error)
        { 
            next(error);
            return;
        }
      res.json(results);
    });
   // res.send('respond with a resource');
  });
  

/* GET spesific author books by author id */
router.get('/book/:id',validator.fields(validationSchema), function(req, res, next) {
    sql.query("select * from books where id=?",[req.params.id],function(error,results,fields){
        if(error)
        { 
            next(error);
            return;
        }
      res.json(results);
    });
   // res.send('respond with a resource');
  });
  

module.exports = router;
