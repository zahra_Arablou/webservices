CREATE DATABASE grocery;
use grocery;

CREATE TABLE groceryTbl (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sku_id` VARCHAR(20) NOT NULL,
  `product_name` VARCHAR(40) NOT NULL,
  `exp_date` DATE NOT NULL,
  `days_to_expire_from_today` INT NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `groceryTbl` ( `sku_id`, `product_name`,`exp_date`,`days_to_expire_from_today`) VALUES
( 'ek613','Whole Grain Bread','2021-04-15',20 );

INSERT INTO `groceryTbl` ( `sku_id`, `product_name`,`exp_date`,`days_to_expire_from_today`) VALUES
( 'pb016','Almond Milk','2021-04-18',23 );

UPDATE groceryTbl SET sku_id='tt623', product_name='Whole', exp_date='2020-05-01', days_to_expire_from_today=20 where id=1;
