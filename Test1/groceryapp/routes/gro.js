var express = require('express');
var router = express.Router();
const sql =require('../db');

//GET API to get one product by sku_id
router.get('/:sku_id',function(req,res){
    sql.query('SELECT * FROM groceryTbl WHERE sku_id=?', [req.params.sku_id], function(error, results, fields) {
     if (error) throw error;
     res.json(results);
     });
 });