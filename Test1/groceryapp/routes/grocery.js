var express = require('express');
var router = express.Router();
const sql =require('../db');
//these two line bellow are for validation 
const Joi=require('joi');
const validator =require('express-joi-validation').createValidator({});

//* GET users listing.and by sku_id as well */ 
 
router.get('/', function(req, res, next) {
  let sqlString="select * from groceryTbl";
  if(req.query && req.query.sku_id)
     sqlString+=` where sku_id=${req.query.sku_id}`
  sql.query(sqlString,function(error,results,fields){
    if(error) throw error;
    res.json(results);
  });
});

//validation part
const validationSchema=Joi.object({
  sku_id:Joi.string().min(3).max(30).required(),
  product_name: Joi.string().min(3).max(30).required(),
  exp_date:Joi.required(),
  days_to_expire_from_today:Joi.required(),
});
//POST API to create a new product
router.post('/',validator.body(validationSchema),function(req,res){
 
  //way-1:
     const data=req.body;
     sql.query('INSERT INTO groceryTbl SET ?',data,function(error,results,fields){
         if(error) throw error;
         res.json(results);
      });
       //way-2
       //const sqlString=`INSERT INTO groceryTbl(sku_id, product_name, exp_date,days_to_expire_from_today) VALUES ('${req.body.sku_id}','${req.body.product_name}','${req.body.exp_date}','${req.body.days_to_expire_from_today}')`;
       //sql.query(sqlString,(error,results) => {
       // if(error) throw error;
      // res.json(results);
      //});
  
    });


 //GET API to get one product by sku_id
router.get('/sku/:sku_id',function(req,res){
  sql.query('SELECT * FROM groceryTbl WHERE sku_id=?', [req.params.sku_id], function(error, results, fields) {
   if (error) throw error;
   res.json(results);
   });
});


//GET API to get one product by id
router.get('/:id',function(req,res){
 sql.query('SELECT * FROM groceryTbl WHERE id=?', [req.params.id], function(error, results, fields) {
    if (error) throw error;
       res.json(results);
    });
});


//PUT API to update one product by id
router.put('/:id',function(req,res){
  console.log(req.params);
  sql.query('UPDATE groceryTbl SET sku_id=?, product_name=?, exp_date=?, days_to_expire_from_today=?  where id=?', [req.body.sku_id, req.body.product_name, req.body.exp_date, ,req.body.days_to_expire_from_today,req.params.id], function(error, results, fields) {
  if (error) throw error;
   res.json(results);  
});
});


module.exports = router;
