var express = require('express');
var router = express.Router();

let users=[
  {name:"aaa",age:10},
  {name:"bbb",age:35},
  {name:"nnn",age:45}
]
//users=[]; //can not do this
//users[0].name="somthing else" //allowed

/* GET users listing. */
router.get('/', function(req, res, next) {
 
  //res.send({users:users});
  res.json({users:users});
});

router.post('/',function(req,res){
  users.push(req.body)
 // console.log(req.body);
  //res.json({resp:"request recieved"}) 
  res.json({users:users});
});

router.put('/',function(req,res){
//users.push(req.body.users);

  //way1 - change const to let
  //users=req.body.users;
 // res.json({users: users})

 //way2
 users.length=0;
 user=req.body.users;
user.forEach(u=>{
  users.push(u);
});
res.json({users:users});
})

router.delete('/',function(req,res){
  //way1 
  
//user=[];
 //way2
 users.length=0;
 res.json({users:users});
})

router.delete('/:nameToDelete',function(req,res){
 //finding Index
 console.log(req.params);
 const foundIndex=users.findIndex(user=>user.name===req.params.nameToDelete)

 //check ifthe element was found
 if(foundIndex>=0)
 {
   //delete the element at the found index position
  users.splice(foundIndex,1);
  //return response
  res.json({users:users});
 }
 else{
  res.json({users:users});
 }
//users.forEach(u=>{
  //if(u.name===req.params.nameToDelete

 // {
  //  let index=users.findIndex(u);
// users.splice(index,1);
//}
// res.json({users:users});
//});
})
//In class assignment
//write an api to fetch one specific user by name - get api
router.get('/:nameToFind', function(req, res, next) {
  const foundIndex=users.findIndex(user=>user.name===req.params.nameToFind);
  if(foundIndex<0){
    res.json({resp:"user nor found"});
  }
  else{
   
    res.json({users:users[foundIndex]})
  }
  //res.send({users:users});
  //res.json({users:users});
});
//write an api to edit one specific user by name- put api

router.put('/:heroName',function(req,res){
  const foundIndex=users.findIndex(user=>user.name===req.params.heroName);
  if(foundIndex<0){
    res.json({resp:"user nor found"});
  }
  else{
    users[foundIndex]=req.body;
    res.json({users:users})
  }
});
router.patch('/:heroName',function(req,res){
  const foundIndex=users.findIndex(user=>user.name===req.params.heroName);
  if(foundIndex<0){
    res.json({resp:"user nor found"});
  }
  else{
    if(req.body.name)
    {
      users[foundIndex].name=req.body.name;
    }
    if (req.body.age)
    {
      users[foundIndex].age=req.body.age;
    }
 
    
    res.json({users:users})
  }
});


//Assignment 
//Create api for orders

module.exports = router;
