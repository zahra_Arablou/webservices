const express = require('express');
const router = express.Router();

const orders =[
  {
    id:1,
    productName:"Reebok ahoes",
    price:200,
    color:"black",
    quantity:50
  },
  {
    id:2,
    productName:"sport shirt",
    price:20,
    color:"red",
    quantity:10
  },
]
// All resources
/* GET orders listing. */
//-get api to fetch all ther orders
router.get('/', function(req, res, next) {
  
  //res.send({orders:orders});
  res.json({orders:orders});
});

 // - put api to edit all the orders
router.put('/',function(req,res){
 
   //way2
   orders.length=0;
   order=req.body.orders;
  order.forEach(u=>{
    orders.push(u);
  });
  res.json({orders:orders});
  })

  // - delete api to delete all orders 
  router.delete('/',function(req,res){
    //way1 
   //orders=[];

   //way2
   orders.length=0;
   res.json({orders:orders});
  })

//Each resource
//-get api to fetch one orders by id
router.get('/:id', function(req, res, next) {
  const foundIndex=orders.findIndex(order=>order.id==req.params.id);
  if(foundIndex<0){
    res.json({resp:"order nor found"});
  }
  else{
   
    res.json({orders:orders[foundIndex]})
  }
  //res.send({orders:orders});
  //res.json({orders:orders});
});
//-put api to edit one order by id
router.put('/:id',function(req,res){
  const foundIndex=orders.findIndex(order=>order.id==req.params.id);
  if(foundIndex<0){
    res.json({resp:"order nor found"});
  }
  else{
    orders[foundIndex]=req.body;
    res.json({orders:orders})
  }
});

router.patch('/:id',function(req,res){
  const foundIndex=orders.findIndex(order=>order.id==req.params.id);
  if(foundIndex<0){
    res.json({resp:"order nor found"});
  }
  else{
      if(req.body.id)
    {
      orders[foundIndex].id=req.body.id;
    }
    if (req.body.productName)
    {
      orders[foundIndex].productName=req.body.productName;
    }
    if (req.body.price)
    {
      orders[foundIndex].price=req.body.price;
    }
    if (req.body.color)
    {
      orders[foundIndex].color=req.body.color;
    }
    if (req.body.quantity)
    {
      orders[foundIndex].quantity=req.body.quantity;
    }
    
    res.json({orders:orders})
  }
});

//-delete api to delete one order by id
router.delete('/:id',function(req,res){
  //finding Index
  console.log(req.params);
  const foundIndex=orders.findIndex(order=>order.id==req.params.id)
 
  //check ifthe element was found
  if(foundIndex>=0)
  {
    //delete the element at the found index position
   orders.splice(foundIndex,1);
   //return response
   res.json({orders:orders});
  }
  else{
   res.json({orders:orders});
  }
})

//Post api to create a new order

router.post('/',function(req,res){
  orders.push(req.body)
 // console.log(req.body);
  //res.json({resp:"request recieved"}) 
  res.json({orders:orders});
});

module.exports = router;