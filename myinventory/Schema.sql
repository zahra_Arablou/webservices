CREATE DATABASE inventory;

use inventory;

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `sku_id` varchar(10) NOT NULL UNIQUE,
  `product_name` varchar(200) NOT NULL,
  `expiry_date` datetime NOT NULL
  );
 
ALTER TABLE `products` ADD PRIMARY KEY (`id`);
ALTER TABLE `products` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `products` (`sku_id`, `product_name` , `expiry_date`) VALUES
('iy1234', 'Butter', '2021-03-27' ),
('sd765', 'Yogurt', '2021-03-30' ),
('fu765', 'Candy', '2021-04-02' ),
('vg765', 'jam', '2021-04-27' ),
('hg876', 'milk', '2021-03-28' )

