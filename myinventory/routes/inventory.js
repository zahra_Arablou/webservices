const express = require('express');
const router = express.Router();

const sql = require('../db')


module.exports = router;

//GET API to get the list of all products in inventory
router.get('/', function(req, res, next) {
    sql.query("SELECT id , sku_id  , product_name , DATE_FORMAT(expiry_date, '%d %M, %Y') as 'expiry_date' , DATEDIFF(expiry_date, CURDATE()) as 'days_to_expire_from_today' FROM products", function(error, results, fields) {
        if (error) throw error;
        res.json(results);
    });
});

//POST API to create a new product
router.post('/', function(req, res) {
    const data = req.body;
    sql.query('INSERT INTO products SET ?', data, function(error, results, fields) {
        if (error) throw error;
        res.json(results);
    });
});

// GET API to get one product by id
router.get('/:id', function(req, res) {
    console.log(req.params);
    sql.query("SELECT id , sku_id  , product_name , DATE_FORMAT(expiry_date, '%d %M, %Y') as 'expiry_date' , DATEDIFF(expiry_date, CURDATE()) as 'days_to_expire_from_today' FROM products WHERE id=?", [req.params.id], function(error, results, fields) {
        if (error) throw error;
        res.json(results);
    });
});

//GET API to get one product by sku_id
router.get('/:skuid', function(req, res) {
    console.log(req.params);
    sql.query("SELECT id , sku_id  , product_name , DATE_FORMAT(expiry_date, '%d %M, %Y') as 'expiry_date' , DATEDIFF(expiry_date, CURDATE()) as 'days_to_expire_from_today' FROM products WHERE sku_id =?", [req.params.skuid], function(error, results, fields) {
        if (error) throw error;
        res.json(results);
    });
});

//PUT API to update one product by id
router.put('/:id', function(req, res) {
    console.log(req.params);
    sql.query('UPDATE products SET sku_id=?, product_name=?, expiry_date=? where id=?', [req.body.sku_id, req.body.product_name, req.body.expiry_date, req.params.id], function(error, results, fields) {
        if (error) throw error;
        res.json(results);
    });
});