var express = require('express');
var router = express.Router();
const sql =require('../db');
const Joi=require('joi');
const validator =require('express-joi-validation').createValidator({passError:true});



/* GET users listing. */
router.get('/', function(req, res, next) {
    sql.query("select * from members order by name",function(error,results,fields){
      if(error) throw error;
      res.json(results);
    });
   // res.send('respond with a resource');
  });

  const validationSchema=Joi.object({
    name:Joi.string().min(3).max(30).required(),
    email: Joi.string().regex(/^[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+$/).required()
 
 });
 // POST /users
router.post('/', validator.body(validationSchema), (req, res, next) => {
    sql.query('INSERT into members SET ?', req.body, (error, result) => {
      
      if (error) {
        if (error.code === 'ER_DUP_ENTRY') {
          res.status(400).send({
            message: "User already exists. Please sign in or use another email."
          });
        }
        next(error);
        return;
      }
      res.json(result);
    });
  })

  
module.exports = router;