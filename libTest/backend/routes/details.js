var express = require('express');
var router = express.Router();
const sql =require('../db');
const Joi=require('joi');
const validator =require('express-joi-validation').createValidator({});


/* GET users listing. */
router.get('/', function(req, res, next) {
    sql.query("select * from borrows",function(error,results,fields){
      if(error) throw error;
      res.json(results);
    });
   // res.send('respond with a resource');
  });

  router.get('/book/:id', function(req, res, next) {
    sql.query("select b.name,d.issueDate from borrows as d inner join members as m on d.memberId=m.id inner join books as b on d.bookId=b.id where d.memberId=?",[req.params.id],function(error,results,fields){
      if(error) throw error;
      res.json(results);
    });
   // res.send('respond with a resource');
  });
  router.post('/',function(req,res,next){
    //     //way-1
    //       //const sqlString=`INSERT INTO superheros(name, age, image_url) VALUES ('${req.body.name}','${req.body.age}','${req.body.image_url}')`;
    //     //sql.query(sqlString,(error,results) => {
    //      // if(error) throw error;
    //      res.json(results);
    //     //});
        
        //way-2:
           var data=req.body;
           sql.query('INSERT INTO borrows SET ?',data,function(error,results,fields){
               if(error) throw error;
               res.json(results);
            });
          });
module.exports = router;