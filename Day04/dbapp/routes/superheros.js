var express = require('express');
var router = express.Router();
const sql =require('../db');
const Joi=require('joi');
const validator =require('express-joi-validation').createValidator({});

/* GET users listing. */
router.get('/', function(req, res, next) {
  sql.query("select * from superheros",function(error,results,fields){
    if(error) throw error;
    res.json(results);
  });
 // res.send('respond with a resource');
});
router.get('/:id', function(req, res, next) {
  sql.query("select * from superheros where id=?",[req.params.id],function(error,results,fields){
    if(error) throw error;
    res.json(results);
  });
 // res.send('respond with a resource');
});

const validationSchema=Joi.object({
   name:Joi.string().min(3).max(30).required(),
   age: Joi.number().required(),
   image_url:Joi.required()
});
router.post('/', validator.body(validationSchema), function(req,res,next){
//way-1
  //const sqlString=`INSERT INTO superheros(name, age, image_url) VALUES ('${req.body.name}','${req.body.age}','${req.body.image_url}')`;
//sql.query(sqlString,(error,results) => {
 // if(error) throw error;
 // res.json(results);
//});

//way-2:
   var data=req.body;
   sql.query('INSERT INTO superheros SET ?',data,function(error,results,fields){
       if(error) throw error;
       res.json(results);
    });
  });


  //Delete one superhero by id
  router.delete('/:id',function(req,res){
    //finding Index
    
    sql.query('DELETE FROM superheros WHERE id=?', [req.params.id], function(error, results, fields) {
      if (error) throw error;
      res.end('Record has been deleted!');
      });

  });
 //edit one superhero by id 
  router.put('/:id',function(req,res){
    console.log(req.params);
    //const body=req.body;
    ///const sqlStr=`UPDATE superheros SET name=${body.name}, age=${body.age},image_url=${body.image_url} where id=${req.params.id}`;


    sql.query('UPDATE superheros SET name=?, age=?, image_url=? where id=?', [req.body.name, req.body.age, req.body.image_url, req.params.id], function(error, results, fields) {
    if (error) throw error;
       res.json(results);
 });
});
 
 

module.exports = router;
